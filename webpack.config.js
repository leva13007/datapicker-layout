const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    devServer: {
        contentBase: path.join(__dirname, 'src'),
        compress: true,
        port: 9000,
        host: '0.0.0.0'
    },
    mode: "development",
    entry: "./src",
    output: {
        path: path.resolve(__dirname, "dist"),
    },
    watch: true,
    watchOptions: {
        ignored: /node_modules/,
        aggregateTimeout: 600
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + "/src/index.html",
        }),
        // new ExtractTextPlugin({filename: 'style.css'})
    ]
};
